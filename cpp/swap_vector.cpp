#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swap_vector.h"

using namespace std;

/*
 * class swap_vector
 *
 */

size_t swap_vector::unique_id = 0;

swap_vector::swap_vector () :
	nr_items (0),
	nr_ram_max_items (11),
	nr_ram_items (0),
	fp (NULL)
{
	char fn[4096];
	sprintf (fn, "swp/swap_vector_%ld.swp", unique_id);
	if ((fp = fopen (fn, "w+")) == NULL) {
		cout << "[ERROR] fopen failed" << endl;
		exit (-1);
	}
	//cout << fn << " is created." << endl;
	unique_id++;
}

swap_vector::~swap_vector ()
{
	if (fp) {
		fclose (fp);
	}
}

void swap_vector::clear ()
{
	nr_items = 0;
	num_outgoing.clear ();
}

void swap_vector::reserve (size_t size)
{
	nr_items = size;
	num_outgoing.reserve (get_blksz (size));
}

void swap_vector::resize (size_t size)
{
	if (nr_items < size) {
		nr_items = size;
		num_outgoing.resize (get_blksz (size) + 1, NULL);
	}
}

size_t swap_vector::size ()
{
	return nr_items;
}

void swap_vector::ram_size (size_t ram_size)
{
	nr_ram_max_items = ram_size;
}

size_t* swap_vector::swap_in (size_t blk_idx)
{
	size_t* ptr_data = NULL;

	//cout << "swap_in: " << blk_idx << endl;

	if ((ptr_data = (size_t*)malloc (sizeof (size_t) * BLKSZ)) == NULL) {
		cout << "[ERROR] malloc failed" << endl;
		exit (-1);
	} else {
		memset (ptr_data, 0, BLKSZ * sizeof (size_t));
	}

	fseek (fp, sizeof (size_t) * blk_idx * BLKSZ, SEEK_SET);
	fread (ptr_data, sizeof (size_t), BLKSZ, fp);
	num_outgoing[blk_idx] = ptr_data;
	nr_ram_items++;

	return ptr_data;
}

void swap_vector::swap_out ()
{
	size_t i = 0, max = num_outgoing.size ();
	size_t blk_idx = rand () % max;
	size_t err = 1;

	//cout << "swap_out: " << nr_ram_items << " " << nr_ram_max_items << endl;

	if (nr_ram_items <= nr_ram_max_items)
		return;

	for (i = 0; i < max; i++) {
		size_t* ptr_data = NULL;
		//cout << "swap_out2: " << i << " " << blk_idx << endl;
		if ((ptr_data = num_outgoing[blk_idx]) != NULL) {
			num_outgoing[blk_idx] = NULL;

			fseek (fp, sizeof (size_t) * blk_idx * BLKSZ, SEEK_SET);
			fwrite (ptr_data, sizeof (size_t), BLKSZ, fp);
			fflush (fp);

			//cout << "swap_out: " << blk_idx << endl;

			/*
			if (blk_idx == 240 || blk_idx == 66) {
				cout << "*****" << endl;
				for (int i = 0; i < 4096; i++) {
					cout << "[TEST] 240:" << i << "=>" << ptr_data[i] << endl;
				}
				cout << "*****" << endl;
				cout << "TEST: 2555@240: " << ptr_data[2554] << ", " << ptr_data[2555] << ", " << ptr_data[2556] << endl;
				cout << num_outgoing.size () << endl;
			}
			*/

			free (ptr_data);
			if (nr_ram_items <= 0) {
				cout << "[ERROR] nr_ram_items <= 0" << endl;
				exit (-1);
			}
			nr_ram_items--;
			err = 0;
			break;
		}
		blk_idx++;
		blk_idx %= max;
	}

	if (err == 1) {
		cout << "[ERROR] oops! swap_out failed." << endl;
		exit (-1);
	}
}

void swap_vector::set (size_t idx, size_t val)
{
	size_t blk_idx = get_blksz (idx);
	size_t blk_ofs = get_blkofs (idx);
	size_t* ptr_data = NULL;

	/* get a block */
	if (num_outgoing.size () <= blk_idx) {
		//num_outgoing.resize (blk_idx + 1, NULL);
		resize (idx);
	}

	/*
	if (idx == 272891 || idx == 985595) {
		cout << "TEST2: " << blk_idx << " (" << idx << ")" << endl;
	}
	*/

	ptr_data = num_outgoing[blk_idx];

	if (ptr_data == NULL) {
		/* read the block from the file */
		swap_in (blk_idx);
		if ((ptr_data = num_outgoing[blk_idx]) == NULL) {
			cout << "[ERROR] num_outgoing[blk_idx] == NULL (" << __FILE__ << ")" << endl;
			exit (-1);
		}
		ptr_data[blk_ofs] = val;

		/* write a victim block to the file */
		swap_out ();

		//cout << num_outgoing[blk_idx] << " " << ptr_data[blk_ofs] << endl;
	} else {
		ptr_data[blk_ofs] = val;
	}
}

size_t swap_vector::get (size_t idx)
{
	size_t blk_idx = get_blksz (idx);
	size_t blk_ofs = get_blkofs (idx);
	size_t* ptr_data = NULL;
	size_t ret;

	/* get a block */
	if (num_outgoing.size () - 1 < blk_idx) {
		//cout << num_outgoing.size () << " " << blk_idx << endl;
		return -1;
	}

	ptr_data = num_outgoing[blk_idx];

	if (ptr_data == NULL) {
		/* read the block from the file */
		swap_in (blk_idx);
		if ((ptr_data = num_outgoing[blk_idx]) == NULL) {
			cout << "[ERROR] num_outgoing[blk_idx] == NULL (" << __FILE__ << ")" << endl;
			exit (-1);
		}
		//cout << "get: " << num_outgoing[blk_idx] << endl;
		ret = ptr_data[blk_ofs];

		/* write a victim block to the file */
		swap_out ();
	} else {
		ret = ptr_data[blk_ofs];
	}
	
	return ret; 
}


/*
 * class swap_rows
 *
 */

swap_rows::swap_rows () :
	curr_row (0),
	nr_rows (0)
{
}

swap_rows::~swap_rows ()
{
}

bool swap_rows::init_data (vector< vector<size_t> > &rows) // the rowns of the hyperlink matrix
{
	size_t num_rows = rows.size ();
    vector<size_t>::iterator ci; // current incoming

	if (num_rows == 0)
		return false;

	nr_rows = num_rows;

	for (size_t i = 0; i < num_rows; i++) {
		size_t row_size = rows[i].size ();
		size_t* ptr_data = NULL;
		size_t j = 0;
		FILE* fp = NULL;
		char fn[4096];

		//cout << "row size: " << row_size << endl;

		if ((ptr_data = (size_t*)malloc (row_size * sizeof (size_t))) == NULL) {
			cout << "[ERROR] malloc failed." << endl;
			exit (-1);
		}
		memset (ptr_data, row_size * sizeof (size_t), 0x00);

		for (ci = rows[i].begin(); ci != rows[i].end(); ci++) {
			ptr_data[j++] = *ci;
		}

		sprintf (fn, "swp/rows_%ld.swp", i);
		if ((fp = fopen (fn, "w+")) == NULL) {
			cout << "[ERROR] fopen failed" << endl;
			exit (-1);
		}
		fwrite (ptr_data, sizeof (size_t), row_size, fp);
		fflush (fp);
		fclose (fp);

		free (ptr_data);
		ptr_data = NULL;
	}

	return true;
}

size_t swap_rows::get_nr_rows ()
{
	return nr_rows;
}

bool swap_rows::go_first_row ()
{
	curr_row = 0;

	return true;
}

bool swap_rows::go_next_row ()
{
	if (curr_row + 1 < nr_rows) {
		curr_row++;
		return true;
	}
		
	return false;
}

bool swap_rows::get_row_data (size_t* nr_items, size_t** ptr_data)
{
	char fn[4096];
	FILE* fp;
	size_t fsize = 0;

	/* open a row file */
	sprintf (fn, "swp/rows_%ld.swp", curr_row);
	//cout << fn << endl;
	if ((fp = fopen (fn, "r")) == NULL) {
		cout << "[ERROR] fopen failed" << endl;
		exit (-1);
	}

	/* get the length of the file */
	fseek (fp, 0, SEEK_END); 
	fsize = ftell (fp); 
	//cout << "fsize: " << fsize << endl;
	fseek (fp, 0, SEEK_SET); 
	*nr_items = fsize / sizeof (size_t);

	/* alloc memory; it must be released by a caller */
	*ptr_data = (size_t*)malloc (*nr_items * sizeof (size_t));

	/* read the data from the file */
	fread (*ptr_data, sizeof (size_t), *nr_items, fp);
	fclose (fp);

	return true;
}

