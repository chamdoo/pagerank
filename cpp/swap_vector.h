#include <cstddef>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

#define BLKSZ	4096

class swap_vector {
private:
    vector<size_t*> num_outgoing; // number of outgoing links per column
	size_t nr_items;	// actual items 
	size_t nr_ram_items;
	size_t nr_ram_max_items;
	FILE* fp;

	size_t get_blksz (size_t bytesz) { return bytesz / BLKSZ; };
	size_t get_blkofs (size_t bytesz) { return bytesz % BLKSZ; };

	size_t* swap_in (size_t blk_idx);
	void swap_out ();

public:
	swap_vector ();
	~swap_vector ();

	static size_t unique_id;

	void clear ();
	void reserve (size_t size);
	void resize (size_t size);
	size_t size ();

	void ram_size (size_t ram_size);
	void set (size_t idx, size_t val);
	size_t get (size_t idx);
};

class swap_rows {
private:
	size_t curr_row;
	size_t nr_rows;

public:
	swap_rows ();
	~swap_rows ();

	bool init_data (vector< vector<size_t> > &rows); // the rowns of the hyperlink matrix
	size_t get_nr_rows ();

	bool go_first_row ();
	bool go_next_row ();
	bool get_row_data (size_t* nr_items, size_t** ptr_data);
};
